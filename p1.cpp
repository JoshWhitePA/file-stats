//CSC136
// p1.cpp
//  Author:Joshua White
//  Due: 09/10/2014
/*
 The prupose of this program is to ask the user for a file name, read the file and say how many characters and lines are in it 
 and then sort the word in alphabetical order, removing repeated words.
*/
//  Created by Joshua White on 8/28/14.
//  Copyright (c) 2014 Joshua White. All rights reserved.

#include <iostream>
#include <fstream>
#include <cstdlib>
using namespace std;

//------------Function-Prototypes---------
bool openfile(ifstream &inf,string &filename);
void countCharLines(ifstream &inf,int &numchars, int &nLine);
void wordArray(ifstream &inf, string (&aliasCharHolder)[1000]);
//----------------------------------------

int main()
{
  int numLines;             //Stores the number oflines in text file
  int numChars;             // Stores number of characters in text file
  string filename;          // user input of file name
  int booReturns;           // Stores if function openfile is true or false
  ifstream cfile;           // To pass an ifstream I need an ifstream
  string wordHolder[1000];
 
  do {                                              //Do while loop for the user input of the filename
    cout << "Please enter the name of the file you would like scanned: ";
    getline(cin,filename);                        // Gets users file name
    booReturns = openfile(cfile, filename);      // calls the function to open the file
    cout << endl;
  }
  while (booReturns == 0);                       // If the openfile returns false it will loop
  
  countCharLines(cfile, numChars, numLines);
  cout << endl << numChars << endl << numLines << endl; // Displays the number of lines and the number of characters calculated by countCharLine Function
  cfile.close();
  cfile.clear();
  cfile.open(filename.c_str());
  
  wordArray(cfile, wordHolder);
  
  cfile.close();
  cfile.clear();
  cfile.open(filename.c_str());
  
  return 0;
}

//---------------------Functions-----------

bool openfile( ifstream &inf, string &filename ) // Checks if the file is there returns 1 if true
{
  inf.open( filename.c_str() );
  if (!inf.is_open())
    {
      cout << "The file name: " << filename << " was not found.";
      return false;
      exit(EXIT_FAILURE);// if the file is not found,
    }
  cout << "File opened succesfully";
  return true;
}

void countCharLines( ifstream &inf, int &nChar, int &nLine )
{
  nLine = 1;                        // Adds 1 to the numer of lines because the first line is not denoted by a newline character
  char listchar;
  while (!inf.eof()) {             // Will loop untill end of file
    inf.get(listchar);           // Gets the characters one by one untill EOF
    if (listchar == '\n'){        // Counts New line Characters to add to the line count
      ++nLine;
    }
    nChar++;                    // adds to the number of chars until while loop reaches EOF
  }
}

void wordArray( ifstream &inf, string (&wordHolder)[1000] ){
  int x = 0;
  while (!inf.eof()) {                // Will loop untill end of file
    inf >> wordHolder[x];             // Gets the individual words one by one until EOF
    x++;
  }
}
